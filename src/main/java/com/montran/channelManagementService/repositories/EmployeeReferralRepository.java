package com.montran.channelManagementService.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.montran.channelManagementService.entities.EmployeeReferralChannel;

@Repository
public interface EmployeeReferralRepository extends JpaRepository<EmployeeReferralChannel, Long>{

	EmployeeReferralChannel findByUniqueAccessToken(String accessToken);
	
	EmployeeReferralChannel findByReferrEmployeeIdAndReferenceDateAndReferenceTime(String referrEmployeeId, Date date,String time);
	
}
