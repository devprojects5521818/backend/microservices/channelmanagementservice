package com.montran.channelManagementService.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.montran.channelManagementService.entities.PlacementDriveChannel;

@Repository
public interface PlacementDriveRepository extends JpaRepository<PlacementDriveChannel, Long>{
	
	
	PlacementDriveChannel findByUniqueAccessToken(String accessToken);
	
	PlacementDriveChannel findByCollegeName(String collegeName);
	
	PlacementDriveChannel findByCollegeNameAndPlacementDateAndPlacementTime(String collegeName, Date date,String time);

}
