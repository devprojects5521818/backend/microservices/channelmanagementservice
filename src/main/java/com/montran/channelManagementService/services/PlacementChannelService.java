package com.montran.channelManagementService.services;

import java.util.List;

public interface PlacementChannelService {

	List<Long> findCandidateIdsByCollegeName(String collegeName);
	
}
