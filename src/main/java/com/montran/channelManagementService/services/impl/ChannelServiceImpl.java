package com.montran.channelManagementService.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.montran.channelManagementService.entities.EmployeeReferralChannel;
import com.montran.channelManagementService.entities.PlacementDriveChannel;
import com.montran.channelManagementService.http.response.GenericResponse;
import com.montran.channelManagementService.repositories.EmployeeReferralRepository;
import com.montran.channelManagementService.repositories.PlacementDriveRepository;
import com.montran.channelManagementService.services.ChannelService;

@Service
public class ChannelServiceImpl implements ChannelService {
	
	@Value("${placementChannelIdentifierPrefix}")
	private String placementChannelIdentifierPrefix;
	
	@Value("${employeeReferralChannelIdentifierPrefix}")
	private String employeeReferralChannelIdentifierPrefix;
	
	@Value("${hrInitiatedChannelIdentifierPrefix}")
	private String hrInitiatedChannelIdentifierPrefix;
	
	@Value("${candidateWalkInChannelIdentifierPrefix}")
	private String candidateWalkInChannelIdentifierPrefix;
	
	@Value("${consultanyChannelIdentifierPrefix}")
	private String consultanyChannelIdentifierPrefix;
	
	@Autowired private PlacementDriveRepository placementDriveChannelRepo;
	@Autowired private EmployeeReferralRepository empRefRepo;

	@Override
	public GenericResponse<?> findChannelByUniqueAccessToken(String uniqueAccessToken,Long candidateId) {
		if (uniqueAccessToken.startsWith(placementChannelIdentifierPrefix)) {
			return fetchDataFromPlacementRepository(uniqueAccessToken, candidateId);
		} else if (uniqueAccessToken.startsWith(employeeReferralChannelIdentifierPrefix)) {
			return fetchDataFromEmployeeRepository(uniqueAccessToken, candidateId);
		} else if (uniqueAccessToken.startsWith(hrInitiatedChannelIdentifierPrefix)) {
			return GenericResponse.builder().message("Coming Soon").code("OK").body(null).build();
		} else if (uniqueAccessToken.startsWith(candidateWalkInChannelIdentifierPrefix)) {
			return GenericResponse.builder().message("Coming Soon").code("OK").body(null).build();
		} else if (uniqueAccessToken.startsWith(consultanyChannelIdentifierPrefix)) {
			return GenericResponse.builder().message("Coming Soon").code("OK").body(null).build();
		} else {
			return GenericResponse.builder().message("Invalid Condition").code("OK").body(null).build();
		}
	}

	private GenericResponse<?> fetchDataFromPlacementRepository(String uniqueAccessToken, Long candidateId) {
		PlacementDriveChannel placementDrive = placementDriveChannelRepo.findByUniqueAccessToken(uniqueAccessToken);
		placementDrive.getCandidateIdList().add(candidateId);
		PlacementDriveChannel savedPlacementDriveCHannel = placementDriveChannelRepo.save(placementDrive);
		return GenericResponse.builder().message("Candidate Source Updated Successfully").code("OK").body(savedPlacementDriveCHannel).build();
	}

	private GenericResponse<?> fetchDataFromEmployeeRepository(String uniqueAccessToken, Long candidateId) {
		EmployeeReferralChannel employeeRef = empRefRepo.findByUniqueAccessToken(uniqueAccessToken);
		employeeRef.setCandidateId(candidateId);
		empRefRepo.save(employeeRef);
		return GenericResponse.builder().message("Candidate Source Updated Successfully").code("OK").body(employeeRef).build();
	}
	
}
