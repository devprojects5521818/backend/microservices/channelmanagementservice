package com.montran.channelManagementService.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.montran.channelManagementService.entities.PlacementDriveChannel;
import com.montran.channelManagementService.repositories.PlacementDriveRepository;
import com.montran.channelManagementService.services.PlacementChannelService;

@Service
public class PlacementChannelServiceImpl implements PlacementChannelService {
	
	@Autowired private PlacementDriveRepository placementDriveChannelRepo;

	@Override
	public List<Long> findCandidateIdsByCollegeName(String collegeName) {
		PlacementDriveChannel placementDrive = placementDriveChannelRepo.findByCollegeName(collegeName);
		return placementDrive.getCandidateIdList();
	}

}
