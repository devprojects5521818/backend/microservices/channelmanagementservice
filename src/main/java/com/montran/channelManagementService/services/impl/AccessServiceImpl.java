package com.montran.channelManagementService.services.impl;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.montran.channelManagementService.entities.EmployeeReferralChannel;
import com.montran.channelManagementService.entities.PlacementDriveChannel;
import com.montran.channelManagementService.enums.CustomResponseCode;
import com.montran.channelManagementService.http.response.GenericResponse;
import com.montran.channelManagementService.repositories.EmployeeReferralRepository;
import com.montran.channelManagementService.repositories.PlacementDriveRepository;
import com.montran.channelManagementService.services.AccessService;

@Service
public class AccessServiceImpl implements AccessService {
	
	@Value("${registrationPortalUrl}")
	private String registrationPortalUrl;
	
	@Value("${placementChannelIdentifierPrefix}")
	private String placementChannelIdentifierPrefix;
	
	@Value("${employeeReferralChannelIdentifierPrefix}")
	private String employeeReferralChannelIdentifierPrefix;
	
	@Value("${hrInitiatedChannelIdentifierPrefix}")
	private String hrInitiatedChannelIdentifierPrefix;
	
	@Value("${candidateWalkInChannelIdentifierPrefix}")
	private String candidateWalkInChannelIdentifierPrefix;
	
	@Value("${consultanyChannelIdentifierPrefix}")
	private String consultanyChannelIdentifierPrefix;
	
	@Autowired private PlacementDriveRepository placementDriveChannelRepo;
	@Autowired private EmployeeReferralRepository empRefRepo;
	
	private static final Logger log = LoggerFactory.getLogger(AccessServiceImpl.class);
	
	/**
	 * Generate Link for Placement Channel Data
	 */
	@Override
	public GenericResponse<?> generateLinkByPlacementChannelData(PlacementDriveChannel placementDriveChannel) {
		try {
			/**
			 * Technical Validation
			 */
			String errMsg = validatePlacementDriveDetails(placementDriveChannel);
			
			if (errMsg != null) {
				return GenericResponse.builder().code(CustomResponseCode.ERR.name()).message(errMsg).body(null).build();
			}
			
			/**
			 * check if already Placement Drive CHannel exists , if yes return the same link
			 */
			PlacementDriveChannel pdc = placementDriveChannelRepo.findByCollegeNameAndPlacementDateAndPlacementTime(placementDriveChannel.getCollegeName(), placementDriveChannel.getPlacementDate(), placementDriveChannel.getPlacementTime());
			
			/**
			 * Drive is Already Scheduled and Access link is Already Generated
			 */
			if (pdc != null) {
				String accessUrl = generateRegistrationUrlWithUniqueAccessToken(UUID.fromString(pdc.getUniqueAccessToken().substring(3)),placementChannelIdentifierPrefix);
				return GenericResponse.builder().code(CustomResponseCode.WARN.name()).message("PlacementDrive is Already Scheduled").body(accessUrl).build();
			} else {
				/**
				 * Drive Record does not exist hence, inserting a new record 
				 */
				UUID uid = UUID.randomUUID();
				String accessUrl = generateRegistrationUrlWithUniqueAccessToken(uid,placementChannelIdentifierPrefix);
				placementDriveChannel.setUniqueAccessToken(placementChannelIdentifierPrefix + "-" + uid.toString());
				placementDriveChannelRepo.save(placementDriveChannel);
				return GenericResponse.builder().code(CustomResponseCode.OK.name()).message("Link Generated Successfully").body(accessUrl).build();
			}
		} catch (Exception e) {
			log.error("Unable to Generate link for Placement Drive for College " + placementDriveChannel.getCollegeName() , e);
			return GenericResponse.builder().code(CustomResponseCode.ERR.name()).message("Unable To Generate Link").body(null).build();
		}
	}

	private String validatePlacementDriveDetails(PlacementDriveChannel placementDriveChannel) {
		if (placementDriveChannel.getCollegeName() == null || placementDriveChannel.getCollegeName().isEmpty()) {
			return "Please Enter a Valid College Name";
		} else if (placementDriveChannel.getCollegeAddress() == null || placementDriveChannel.getCollegeAddress().isEmpty()) {
			return "Please Enter a Valid College Address";
		} else if (placementDriveChannel.getPlacementTime() == null || placementDriveChannel.getPlacementTime().isEmpty()) {
			return "Please Enter a Valid Placement Drive Time";
		} else if (placementDriveChannel.getPlacementDate() == null) {
			return "Please Enter a Valid Placement Drive Date";
		} else {
			return null;
		}
	}
	
	
	private String validateEmployeeReferralDetails(EmployeeReferralChannel employeeReferralChannel) {
		if (employeeReferralChannel.getReferrEmployeeId() == null || employeeReferralChannel.getReferrEmployeeId().isEmpty()) {
			return "Please Enter a Valid Employee Id";
		} else if (employeeReferralChannel.getReferrerEmployeeName() == null || employeeReferralChannel.getReferrerEmployeeName().isEmpty()) {
			return "Please Enter a Valid Employee Name";
		} else if (employeeReferralChannel.getReferenceTime() == null || employeeReferralChannel.getReferenceTime().isEmpty()) {
			return "Please Enter a Valid Referral Time";
		} else if (employeeReferralChannel.getReferenceDate() == null) {
			return "Please Enter a Valid Referral Date";
		} else {
			return null;
		}
	}

	/**
	 * Generate Link for Employee Referral
	 */
	@Override
	public GenericResponse<?> generateLinkByEmployeeRefChannelData(EmployeeReferralChannel employeeReferralChannel) {
		try {
			/**
			 * Technical Validation
			 */
			String errMsg = validateEmployeeReferralDetails(employeeReferralChannel);
			
			if (errMsg != null) {
				return GenericResponse.builder().code(CustomResponseCode.ERR.name()).message(errMsg).body(null).build();
			}
			
			/**
			 * check if Employee Referral Access Link is Already generated, if yes return the same Link
			 */
			EmployeeReferralChannel empRefChannel = empRefRepo.findByReferrEmployeeIdAndReferenceDateAndReferenceTime(employeeReferralChannel.getReferrEmployeeId(), employeeReferralChannel.getReferenceDate(), employeeReferralChannel.getReferenceTime());
			
			if (empRefChannel != null) {
				String accessUrl = generateRegistrationUrlWithUniqueAccessToken(UUID.fromString(empRefChannel.getUniqueAccessToken().substring(3)),employeeReferralChannelIdentifierPrefix);
				return GenericResponse.builder().code(CustomResponseCode.WARN.name()).message("Access Url on Employee Referral Already Exists").body(accessUrl).build();
			} else {
				UUID uid = UUID.randomUUID();
				String accessUrl = generateRegistrationUrlWithUniqueAccessToken(uid,employeeReferralChannelIdentifierPrefix);
				employeeReferralChannel.setUniqueAccessToken(employeeReferralChannelIdentifierPrefix + "-" + uid.toString());
				empRefRepo.save(employeeReferralChannel);
				return GenericResponse.builder().code(CustomResponseCode.OK.name()).message("Link Generated Successfully").body(accessUrl).build();
			}
		} catch (Exception e) {
			log.error("Unable to Generate link for Employee Referral " + employeeReferralChannel.getReferrEmployeeId() , e);
			return GenericResponse.builder().code(CustomResponseCode.ERR.name()).message("Unable To Generate Link").body(null).build();
		}
	}
	
	/**
	 * Utility Method for generating access Url
	 */
	private String generateRegistrationUrlWithUniqueAccessToken(UUID uid,String channelIdentifierPrefix) {
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(registrationPortalUrl).append(channelIdentifierPrefix).append("-").append(uid);
		log.debug("Generated Url : " + urlBuilder.toString());
		return urlBuilder.toString();
	}

}
