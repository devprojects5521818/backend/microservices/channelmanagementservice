package com.montran.channelManagementService.services;

import com.montran.channelManagementService.http.response.GenericResponse;

public interface ChannelService {
	
	public GenericResponse<?> findChannelByUniqueAccessToken(String uniqueAccessToken,Long candidateId);

}
