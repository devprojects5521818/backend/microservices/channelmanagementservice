package com.montran.channelManagementService.services;

import com.montran.channelManagementService.entities.EmployeeReferralChannel;
import com.montran.channelManagementService.entities.PlacementDriveChannel;
import com.montran.channelManagementService.http.response.GenericResponse;

public interface AccessService {
	
	public GenericResponse<?> generateLinkByPlacementChannelData(PlacementDriveChannel placementDriveChannel);
	
	public GenericResponse<?> generateLinkByEmployeeRefChannelData(EmployeeReferralChannel employeeReferralChannel);
	


}
