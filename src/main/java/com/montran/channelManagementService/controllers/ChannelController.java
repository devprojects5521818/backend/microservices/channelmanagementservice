package com.montran.channelManagementService.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.montran.channelManagementService.http.response.GenericResponse;
import com.montran.channelManagementService.services.ChannelService;
import com.montran.channelManagementService.services.PlacementChannelService;

@RestController
@RequestMapping("/channel")
@CrossOrigin(allowedHeaders = "*")
public class ChannelController {
	
	@Autowired private PlacementChannelService placementChannelService;
	@Autowired private ChannelService channelService;
	
	@GetMapping("/getByCollegeName/{collegeName}")
	public List<Long> getCandidateIdsFromCollegeName(@PathVariable("collegeName") String collegeName) {
		return placementChannelService.findCandidateIdsByCollegeName(collegeName); 
	}
	
	@GetMapping("/fetchByAccessToken/{uniqueAccessToken}/{candidateId}")
	public GenericResponse<?> fetchChannelByAccessToken(@PathVariable ("uniqueAccessToken") String uniqueAccessToken,@PathVariable("candidateId") Long candidateId) {
		return channelService.findChannelByUniqueAccessToken(uniqueAccessToken, candidateId);
	}

}
