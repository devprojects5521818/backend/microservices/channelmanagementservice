package com.montran.channelManagementService.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.montran.channelManagementService.entities.EmployeeReferralChannel;
import com.montran.channelManagementService.entities.PlacementDriveChannel;
import com.montran.channelManagementService.http.response.GenericResponse;
import com.montran.channelManagementService.services.AccessService;

@RestController
@RequestMapping("/access")
@CrossOrigin(allowedHeaders = "*")
public class AccessController {
	
	@Autowired private AccessService accessService;
	
	@PostMapping("/link/generate/empRef")
	public GenericResponse<?> generateAccessLink(@RequestBody EmployeeReferralChannel employeeReferralChannel) {
		return accessService.generateLinkByEmployeeRefChannelData(employeeReferralChannel);
	}
	
	@PostMapping("/link/generate/placementDrive")
	public GenericResponse<?> generateAccessLink(@RequestBody PlacementDriveChannel placementDriveChannel) {
		return accessService.generateLinkByPlacementChannelData(placementDriveChannel);
	}

}
