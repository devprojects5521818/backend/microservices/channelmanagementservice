package com.montran.channelManagementService.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor @NoArgsConstructor
@Data @Builder
public class EmployeeReferralChannel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * Unique Id for user to Access the Registration Portal
	 */
	private String uniqueAccessToken;  
	private String referrEmployeeId;
	private String referrerEmployeeName;
	private Date referenceDate;
	private String referenceTime;
	private Long candidateId;

}
