package com.montran.channelManagementService.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor @NoArgsConstructor
@Data
public class PlacementDriveChannel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 883925685617029819L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String collegeName;
	private Date placementDate;
	private String placementTime;
    private String collegeAddress;
    @ElementCollection
    private List<Long> candidateIdList = new ArrayList<Long>();
    private String uniqueAccessToken;
}
